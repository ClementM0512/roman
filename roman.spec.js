const {toRoman} = require('./roman.js')
/** 10 FIRST NUMBER */
describe('toRoman', () =>{
    it('return I when given 1', () => {
        expect(toRoman(1)).toEqual('I')
    })
})
describe('toRoman', () =>{
    it('return II when given 2', () => {
        expect(toRoman(2)).toEqual('II')
    })
})
describe('toRoman', () =>{
    it('return III when given 3', () => {
        expect(toRoman(3)).toEqual('III')
    })
})
describe('toRoman', () =>{
    it('return IV when given 4', () => {
        expect(toRoman(4)).toEqual('IV')
    })
})
describe('toRoman', () =>{
    it('return V when given 5', () => {
        expect(toRoman(5)).toEqual('V')
    })
})
describe('toRoman', () =>{
    it('return VI when given 6', () => {
        expect(toRoman(6)).toEqual('VI')
    })
})
describe('toRoman', () =>{
    it('return VII when given 7', () => {
        expect(toRoman(7)).toEqual('VII')
    })
})
describe('toRoman', () =>{
    it('return VIII when given 8', () => {
        expect(toRoman(8)).toEqual('VIII')
    })
})
describe('toRoman', () =>{
    it('return IX when given 9', () => {
        expect(toRoman(9)).toEqual('IX')
    })
})
describe('toRoman', () =>{
    it('return X when given 10', () => {
        expect(toRoman(10)).toEqual('X')
    })
})
/** SLY NUMBER */
describe('toRoman', () =>{
    it('return XLII when given 42', () => {
        expect(toRoman(42)).toEqual('XLII')
    })
})
describe('toRoman', () =>{
    it('return DCLXVI when given 666', () => {
        expect(toRoman(666)).toEqual('DCLXVI')
    })
})
describe('toRoman', () =>{
    it('return CMXCIX when given 999', () => {
        expect(toRoman(999)).toEqual('CMXCIX')
    })
})
describe('toRoman', () =>{
    it('return CCCLXXXVII when given 387', () => {
        expect(toRoman(387)).toEqual('CCCLXXXVII')
    })
})
/** NEGATIF NUMBER */
describe('toRoman', () =>{
    it('return number when given negatif number', () => {
        expect(toRoman(-5)).toEqual(-5)
    })
})