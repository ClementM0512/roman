document.addEventListener("keydown", function(event) {
    ControleSaisie(event);
});

/**
 * Contrôle la saisie du formulaire
 * @param {*} e
 */
function ControleSaisie(e) {
    document.getElementById("erreur_saisie").innerHTML = "";
    const regex = new RegExp("^[0-9]{1,}$");
    let saisie = "";

    /**
     * KeyCode de 48 à 90 ([0-9] et [a-z])
     * KeyCode de 96 à 105 ([0-9] => pavé numérique)
     */
    if (
        (e.keyCode >= 48 && e.keyCode <= 90) ||
        (e.keyCode >= 96 && e.keyCode <= 105)
    ) {
        saisie = e.srcElement.value + e.key;
    } else {
        /**
         * KeyCode : 8 => touche backspace
         */
        if (e.keyCode == 8) {
            saisie = document.getElementById("nombre_arabe").value;
            let taillePrec = saisie.length;
            saisie = saisie.substring(0, taillePrec - 1);
            console.log(saisie);
        }
    }

    let controleOk = regex.test(saisie);

    if (!controleOk) {
        if (saisie.length == 0) {
            document.getElementById("erreur_saisie").innerHTML =
                "Erreur de saisie, veuillez entrer une nombre entier";
        } else {
            document.getElementById("erreur_saisie").innerHTML =
                "Erreur de saisie, " + saisie + " n'est pas un nombre entier";
        }
        document.getElementById("btn_valid").disabled = true;
    } else {
        document.getElementById("btn_valid").disabled = false;
    }
}
